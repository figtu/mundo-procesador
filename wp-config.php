<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'procesadores' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', '' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         '-PO|,`1_kZUV>sw`J%>r)(Lo6VUmQ9!>VT_Z4%)@1{F41A1N!dvSE$1,#$x<QlI ' );
define( 'SECURE_AUTH_KEY',  'JPOZZN2.3)QLf}$wXZx q*,GuY)[t_v(2F9LWF*dnF$k!I<,:c(^vW(Q,1^lLP~5' );
define( 'LOGGED_IN_KEY',    '&6<}f)3[[1s5-C()m8G@LWX/7P&Zjv]V|Pv( di<jW?IZ.pOeP9<XTOHVW&#;b%E' );
define( 'NONCE_KEY',        'uHv52v5y4x#a;V7kEMjRkOy)P;8}iJpSKfnR>m4Ng^S6!L>@uP-}O~]wsCUO#+CC' );
define( 'AUTH_SALT',        '{.FD*^~w`@X:94xMPC;fqyo>JxNpmJRI}AKD@t0q:eqAaxd;OgO#(DMD< _7z .t' );
define( 'SECURE_AUTH_SALT', '`.[E/~wZ!2T-Tv8rmS~^fov@{ct4fs^Tohl^d$NOxSNMG|2;O.2SKqssZ,712 w(' );
define( 'LOGGED_IN_SALT',   '1={m}dq-bztA}<0l!1v*{~0xI4_s}^[>*5JN.%2})yG}BVK3~@lzA6KnGO!tQA+,' );
define( 'NONCE_SALT',       '_t4 L&~^TDc[oMTzaFmszDpl`M/u$6|r%YpuqN:YTTF26 k&{)RX19Pel&u%9EqF' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the documentation.
 *
 * @link https://wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
